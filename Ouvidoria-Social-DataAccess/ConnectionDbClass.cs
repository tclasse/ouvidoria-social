﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace Ouvidoria_Social_DataAccess
{
    public class ConnectionDbClass
    {
        //LOCAL
        public static string StringConexao ="persist security info=False;server=localhost;database=os_database;uid=root;pwd=admin";

        //AMAZON OFICIAL
        //public static string StringConexao ="persist security info=False;server=mysqltadeu.clr2njwaayrb.us-west-2.rds.amazonaws.com;database=os_database;uid=root;pwd=tadeuclasse";

        //AMAZON TREINO
        //public static string StringConexao = "persist security info=False;server=mysqltadeu.clr2njwaayrb.us-west-2.rds.amazonaws.com;database=os_database_treino;uid=root;pwd=tadeuclasse";

        public static string CaminhoImagens = "\\Images";

        private static ModeloBanco _instance = null;
        
        public static ModeloBanco DataModel
        {
            get
            {
                var osConfig = LerConfiguracoes();

                if (CreateDataBase(osConfig))
                {
                    StringConexao ="persist security info=False;server="+ osConfig.DataBaseAddres +
                                   ";database=os_database;uid="+ osConfig.DataBaseUser+
                                   ";pwd="+ osConfig.DataBasePsw + ";port="+ osConfig.DataBasePort +";";
                    
                    return _instance ?? (_instance = new ModeloBanco(StringConexao));
                }
                else
                {
                    return null;
                }
            }
        }

        private static OsConfig LerConfiguracoes()
        {
            //var ass = Assembly.GetExecutingAssembly();
            //var configStream = new StreamReader(ass.GetManifestResourceStream("Ouvidoria_Social_DataAccess.OsConfig.json"));
            //var configFile = configStream.ReadToEnd();

            var configFile = File.ReadAllText(HttpContext.Current.Server.MapPath("~/OsConfig.json"));
                
            var osConfig = JsonConvert.DeserializeObject<OsConfig>(configFile);

            return osConfig;
        }

        private static bool CreateDataBase(OsConfig osConfig)
        {
            try
            {
                var command = "server="+ osConfig.DataBaseAddres +";user="+ osConfig.DataBaseUser +
                              ";port="+ osConfig.DataBasePort +";password="+ osConfig.DataBasePsw +";";

                var conn = new MySqlConnection(command);
                var cmd = conn.CreateCommand();

                conn.Open();

                cmd.CommandText = "SHOW DATABASES LIKE 'os_database'";
                var record = cmd.ExecuteScalar();
                
                if (record == null)
                {
                    var ass = Assembly.GetExecutingAssembly();
                    var configStream =
                        new StreamReader(ass.GetManifestResourceStream("Ouvidoria_Social_DataAccess.SQL.OsDbSchema.sql"));
                    var sql = configStream.ReadToEnd();
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();

                    configStream =
                        new StreamReader(ass.GetManifestResourceStream("Ouvidoria_Social_DataAccess.SQL.OsDbData.sql"));
                    sql = configStream.ReadToEnd();
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();
                }

                conn.Close();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
